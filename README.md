모라이 시뮬레이터 내 CAN 통신의 Data를 시각화 할 수 있는 기능으로 Input Data가 Output Data로 명확히 표현이 되는지 확인 하는 테스터 입니다.
1. CAN Driver 설치
  - OS에 맞는 KaserCAN Driver를 설치  - 설치 후 Morai SIM 재실행
2. Morai SIM에서 Map과 사용할 Vehicle 선택[V_RHT_StraightHighway_2 , Genesis G80]
3. CAN 통신 활성화
CAN 통신 네트워크 지정, 경로 설정 지정.
4. Python 으로 시나리오 실행
 - Pip install python-can
 - Pip install tabulate
 - python visualize.py 실행
정확도 확인 방법:

가) Input command:
1. 방향 지시등
2. Throttle 
3. Brake
4. Steering Angle 
5. Gear change

나) 시뮬레이션 시각화 요소:
1. Ego vehicle Status Driving info.
2. Ego Cluster
3. Ego vehicle Scene View

다) 시각화 요소의 정확성 검증
1. Terminal를 통해 사용자가 입력한 값이 정보를 시각화한 Terminal에 잘 표현되는지 항목별로 캡처를 통해 확인.
2. Terminal 창의 상단 부분 TimeStamp를 통해 확인
1. 입력/출력 간의 차이가 있는지 검증

입력한 Command가 두 그래프 모두 같은 y값을 표기 되는지 확인. 
